$(document).ready(function () {

    $("#count_form").validate({
    rules: {
        text_words: {
          required: true,
          minlength: 1,
          maxlength: 10000
        },
      },
     messages: {
                text_words: {required: "Enter your text 1-10 characters"},
          },
 });