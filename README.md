# Rafael Prenzier Voxy Test

Here is my solution for the voxy word count test

### Clonning

In your computer go to terminal and type

```
git clone https://bitbucket.org/rprenzier/voxy-test.git
```

### Installing

After cloning the repository run the bundle install to install all necessaries gems

```
bundle install
```

## Running the app

On the root directory run the rackup

```
rackup
```

### Opening

go to the web broser and enter the url

```
localhost:9292
```

### Done

Enjoy :)

## Built With

* [Sinatra](http://www.sinatrarb.com/) - The micro web framework used
* [Jquery](https://www.jquery.com/) - JavaScript framework
* [Ruby](https://www.ruby-lang.org/) - Ruby Programming Language

## Authors

* **Rafael Prenzier dos Santos**

## License

This project is licensed under the MIT License