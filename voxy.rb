require 'rubygems'
require 'sinatra'

helpers do
	def count_words(text)
		text = text.strip #clean the white space from the begining and end of the text
		count = text.split(/\s+/) #split the text words
    	count.size
	end
end

get '/' do
	#"First Test on Heroku!"
	erb :index
end

post '/count' do
	count = count_words(params[:text_words])
	erb :count, :locals => {:text => params[:text_words], :count => count}
end


